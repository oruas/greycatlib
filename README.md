# GreycatLIB

This project aims at creating a shared librairy providing bindings to greycat.
The current goal is to compile it for a arm64-v8a architecture.
The greycat side is done in the file project.gcl: it provides the functions used to interact with the graph/database.
The C functions used to bind are in src/main.c.
Greycat needs to be installed.
First the file project.gcl is compiled by greycat (greycat build).
The resulting file is used later and run into a VM run by the c functions.
The c functions are using a file named libgreycat.a, obtained when installing greycat.


To use the project:
1) compile the greycat project (project.gcl):

greycat build

2) compile the C sources.

mkdir -p build && cd build && cmake .. && make && cd ..


(Obsolete) 3) run the executable with the bin:
./build/greycatLIB gcdata/project.gcp
