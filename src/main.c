#include <string.h>

#include <gc/machine.h>

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

//to print in the console
#include <android/log.h>

//to print names of files in directory
#include<dirent.h>
#include <errno.h>
//used to read files
#define MAX_LEN 256






void print(char* mystring) {
//    printf(mystring);
//    printf("%s",mystring);
    __android_log_print(ANDROID_LOG_DEBUG, "flutter", mystring,"");
}
void print_int(int error_number) {
    __android_log_print(ANDROID_LOG_DEBUG, "flutter", "The value of the error is %d",error_number);
    //printf("The value of the error is %d",error_number);
}


i32_t* res_code;
gc_program_t* program;
gc_machine_t* machine;
gc_machine_frame_t* frame;
FILE *fp;
FILE *results_file;
char* addProjectFile = "/storage/emulated/0/greycat/project.gcp";
char* addBinFile = "/storage/emulated/0/greycat/data.bin";

void initProjectAdd(char* add) {
    addProjectFile = add;
}
void initBinAdd(char* add) {
    addBinFile = add;
}


void readResultsFile() {
    if (results_file==NULL) {
        print("ERROR: results_file NULL");
    }
    char buffer[MAX_LEN];
    // -1 to allow room for NULL terminator for really long string
    while (fgets(buffer, MAX_LEN - 1, results_file))
    {
        // Remove trailing newline
        buffer[strcspn(buffer, "\n")] = 0;
        print(buffer);
    }
}

int printDirs() {
    struct dirent *de;  // Pointer for directory entry
    // opendir() returns a pointer of DIR type.
    DIR *dr = opendir("/storage/emulated/0/");
    if (dr == NULL)  // opendir returns NULL if couldn't open directory
    {
      print("Could not open current directory");
      return 0;
    }
    // Refer http://pubs.opengroup.org/onlinepubs/7990989775/xsh/readdir.html
    // for readdir()
    while ((de = readdir(dr)) != NULL)
    {
      print(de->d_name);
    }
    closedir(dr);
    return 1;
}


//void init(char *add_file) {
void init() {

    fopen("/storage/emulated/0/greycat/results.temp","w");
//    print_int(errno);
    results_file = fopen("/storage/emulated/0/greycat/results.temp","r");

//    print("PRINTING IN GC (C side) OK\n");
//    print(add_file);
//    char* add_file = "/storage/emulated/0/greycat/project.gcp";
//    char* add_file = "project.gcp";
    program = malloc(sizeof(gc_program_t));
    machine = malloc(sizeof(gc_machine_t));
    res_code = malloc(sizeof(i32_t));

    memset(program, 0, sizeof(gc_program_t));

    //open program file (could also be in-memory file)
//    FILE *fp = fopen(add_file, "r");
    fp = fopen(addProjectFile, "r");
    if (fp == NULL) {
        print("errorno after fopen lib failed:");
        print_int(errno);
        /*
        char cwd[PATH_MAX];
        if (getcwd(cwd, sizeof(cwd)) != NULL) {
           print("Current working dir:");
           print(cwd);
        }
        else {
            print("ERROR GETCWD\n");
        }
        printDirs();
        */
        print(addProjectFile);
//        print("\n");
        print("COULD NOT OPEN FILE\n");
        *res_code = 2;
        goto end;
    }
    //load compiled program
    if (!gc_program__restore(program, fp)) {
        print("COULD NOT LOAD PROGRAM\n");
        *res_code = 3;
        goto end;
    }
    //resolve entrypoint
    u32_t fun_off = gc_program__resolve_entrypoint(program, "init");
    if (fun_off == 0) {
        print("COULD NOT RESOLVE ENTRYPOINT\n");
        *res_code = 4;
        goto end;
    }
    //link greycat internal functions
    if (!gc_program__link(program, NULL)) {
        print("COULD NOT LINK INTERNAL FUNCTIONS\n");
        *res_code = 5;
        goto end;
    }

    //create the virtual machine
    machine = gc_machine__create(1000);
    machine->prog = program;
//    if (!gc_store__open(&machine->store, "data.bin", true, false, false, 0, &machine->root)) {
//    if (!gc_store__open(&machine->store, "/storage/emulated/0/greycat/data.bin", true, false, false, 0, &machine->root)) {
//    if (!gc_store__open(&machine->store, addBinFile, true, false, false, 0, &machine->root)) {
    if (!gc_store__open(&machine->store, addBinFile, true, machine)) {
        print("COULD NOT CREATE THE VM\n");
        print_int(errno);
        *res_code = 6;
        goto end;
    }

    frame = gc_machine__new_frame(machine);
    frame->type = gc_machine_frame_type_ops;
    frame->op = (program->ops.data + program->functions.data[fun_off].op_off);
    frame->fun_off = fun_off;

    gc_machine__exec(machine);
    *res_code = machine->err;

    if(frame==NULL) {
        print("FRAME IS NULL AT THE END OF INITGC (before end)\n");
    }


    end:
    if(frame==NULL) {
        print("FRAME IS NULL AT THE END OF INITGC\n");
    }

}

void closeVM(){
    if (machine != NULL) {
        gc_machine__destroy(machine);
    }
    gc_program__finalize(program);
    if (fp != NULL) {
        fclose(fp);
    }
}

/* Function used to make the VM to execute a given function */
void execGCFunction(char* function_name) {
    u32_t fun_off = gc_program__resolve_entrypoint(program, function_name);
    frame->op = (program->ops.data + program->functions.data[fun_off].op_off);
    frame->fun_off = fun_off;
    gc_machine__exec(machine);
    *res_code = machine->err;
}
char* readResults() {
    if (machine->inflight.type != gc_type_null) {
      gc_buffer__clear(&machine->genbuf);
      gc_buffer__add_as_json(&machine->genbuf, machine->inflight.value, machine->inflight.type, machine);
      char * result = malloc(machine->genbuf.size+1);
      memcpy(result, machine->genbuf.data,machine->genbuf.size);
      result[machine->genbuf.size] = '\0';
      return result;
    }
    return "No results found";
}
/* The return value can be accessed by:
    if (machine->inflight.type != gc_type_null) {
      gc_buffer__clear(&machine->genbuf);
      gc_buffer__add_as_json(&machine->genbuf, machine->inflight.value, machine->inflight.type, machine);
      return (machine->genbuf.data); //machine->genbuf.size contains the size of the char* genbuf.data
    }
*/
/* Function used to make the VM to execute a given function WITH PARAMETERS : to do!!!*/
/*
void execGCFunctionParam(char* function_name, const char* buf, int buf_len) {
    u32_t fun_off = gc_program__resolve_entrypoint(program, function_name);
    frame->op = (program->ops.data + program->functions.data[fun_off].op_off);
    frame->fun_off = fun_off;
    gc_machine__parse_json_params(machine, fun_off, buf, buf_len);
    gc_machine__exec(machine);
    *res_code = machine->err;
}*/









/**
* Functions for 'random' benchmark in which a simple timeseries is stored and random values are added
**/

void initRD() {
    execGCFunction("initRD");
}

void addRD(double val) {
    char svalue[50];
    sprintf(svalue, "%f", val);
    setenv("ADD_POINT_VALUE_RD",svalue,1);
    execGCFunction("addRD");
}
void addRDAt(double val, long time) {
    char svalue[50];
    sprintf(svalue, "%f", val);
    char tvalue[50];
    sprintf(tvalue, "%ld", time);
    setenv("ADD_POINT_VALUE_RD",svalue,1);
    setenv("ADD_POINT_TIME_RD",tvalue,1);
    execGCFunction("addRDAt");
}
char* readRD() {
    execGCFunction("readRD");
    return (readResults());
}
char* readRDAt(long time) {
    char stime[50];
    sprintf(stime, "%ld", time);
    setenv("READ_TIME_RD",stime,1);
    execGCFunction("readRDAt");
    return (readResults());
}


/**
* Functions for accelerometer benchmark in which three timeseries are stored and the values of the accelerometer are added
**/

void initAcc() {
    execGCFunction("initAcc");
}

void addAcc(double valX, double valY, double valZ) {
    char svalueX[50];
    sprintf(svalueX, "%f", valX);
    char svalueY[50];
    sprintf(svalueY, "%f", valY);
    char svalueZ[50];
    sprintf(svalueZ, "%f", valZ);
    setenv("ADD_POINT_VALUE_ACC_X",svalueX,1);
    setenv("ADD_POINT_VALUE_ACC_Y",svalueY,1);
    setenv("ADD_POINT_VALUE_ACC_Z",svalueZ,1);
    execGCFunction("addAcc");
}
void addAccAt(double valX, double valY, double valZ, long time) {
    char svalueX[50];
    sprintf(svalueX, "%f", valX);
    char svalueY[50];
    sprintf(svalueX, "%f", valY);
    char svalueZ[50];
    sprintf(svalueX, "%f", valZ);
    char tvalue[50];
    sprintf(tvalue, "%ld", time);
    setenv("ADD_POINT_VALUE_ACC_X",svalueX,1);
    setenv("ADD_POINT_VALUE_ACC_Y",svalueY,1);
    setenv("ADD_POINT_VALUE_ACC_Z",svalueZ,1);
    setenv("ADD_POINT_TIME_ACC",tvalue,1);
    execGCFunction("addAccAt");
}
char* readAcc() {
    execGCFunction("readAcc");
    return (readResults());
}
char* readAccAt(int time) {
    char stime[50];
    sprintf(stime, "%d", time);
    setenv("READ_TIME_ACC",stime,1);
    execGCFunction("readAccAt");
    return (readResults());
}


void addAccInitTime(double valX, double valY, double valZ) {
    char svalueX[50];
    sprintf(svalueX, "%f", valX);
    char svalueY[50];
    sprintf(svalueY, "%f", valY);
    char svalueZ[50];
    sprintf(svalueZ, "%f", valZ);
    setenv("ADD_POINT_VALUE_ACC_X",svalueX,1);
    setenv("ADD_POINT_VALUE_ACC_Y",svalueY,1);
    setenv("ADD_POINT_VALUE_ACC_Z",svalueZ,1);
    execGCFunction("addAccInitTime");
}
char* readAccInitTime() {
    execGCFunction("readAccInitTime");
    return (readResults());
}








/**
* Functions for GPS traces benchmark in which we store a timeseries for each trace of a training set plus the one of the user
**/

void initGPS() {
    execGCFunction("initGPSDS_aux");
}
void loadDatasetGPS(char * add) {
    setenv("ADD_FILE_GPS_DB",add,1);
    execGCFunction("loadDataset_aux");
}

void readFileDB(char * add) {
    setenv("ADD_FILE_GPS_DB",add,1);
    execGCFunction("readFileDB");
}
char* getTraceDB(int userID) {
    char svalue[50];
    sprintf(svalue, "%d", userID);
    setenv("GET_TRACE_GPS_USERID",svalue,1);
    execGCFunction("getTraceDB_aux");
    return (readResults());
}

void addGPSPointDB(int userID, double timestamp, double lat, double lng ) {
    char svalue_userID[50];
    sprintf(svalue_userID, "%d", userID);
    char stime[50];
    sprintf(stime, "%f", timestamp);
    char svalue_lat[50];
    sprintf(svalue_lat, "%f", lat);
    char svalue_lng[50];
    sprintf(svalue_lng, "%f", lng);
    setenv("ADD_POINT_GPS_DB_USERID",svalue_userID,1);
    setenv("ADD_POINT_GPS_DB_TIME",stime,1);
    setenv("ADD_POINT_GPS_DB_LAT",svalue_lat,1);
    setenv("ADD_POINT_GPS_DB_LNG",svalue_lng,1);
    execGCFunction("addGPSPointDB");
}
void addGPSPoint(double timestamp, double lat, double lng) {
    char stime[50];
    sprintf(stime, "%f", timestamp);
    char svalue_lat[50];
    sprintf(svalue_lat, "%f", lat);
    char svalue_lng[50];
    sprintf(svalue_lng, "%f", lng);
    setenv("ADD_POINT_GPS_TIME",stime,1);
    setenv("ADD_POINT_GPS_LAT",svalue_lat,1);
    setenv("ADD_POINT_GPS_LNG",svalue_lng,1);
    execGCFunction("addGPSPoint");
}





void addTripletTime(long time, double valueX, double valueY, double valueZ) {
    char stime[50];
    sprintf(stime, "%ld", time);
    char svalueX[50];
    sprintf(svalueX, "%f", valueX);
    char svalueY[50];
    sprintf(svalueY, "%f", valueY);
    char svalueZ[50];
    sprintf(svalueZ, "%f", valueZ);
    setenv("ADD_POINT_TIME",stime,1);
    setenv("ADD_POINT_VALUE_X",svalueX,1);
    setenv("ADD_POINT_VALUE_Y",svalueY,1);
    setenv("ADD_POINT_VALUE_Z",svalueY,1);
    execGCFunction("addTripletTime");
}
void addTriplet(double valueX, double valueY, double valueZ) {
    char svalueX[50];
    sprintf(svalueX, "%f", valueX);
    char svalueY[50];
    sprintf(svalueY, "%f", valueY);
    char svalueZ[50];
    sprintf(svalueZ, "%f", valueZ);
    setenv("ADD_POINT_VALUE_X",svalueX,1);
    setenv("ADD_POINT_VALUE_Y",svalueY,1);
    setenv("ADD_POINT_VALUE_Z",svalueY,1);
    execGCFunction("addTriplet");
}

void readValue() {
    execGCFunction("read");
    readResultsFile();
}
void readAt(long time) {
    char stime[50];
    sprintf(stime, "%ld", time);
    setenv("READ_TIME",stime,1);
    execGCFunction("readAt");
    readResultsFile();
}



char* returnParam() {
    execGCFunction("returnParam");
    if (machine->inflight.type != gc_type_null) {
      gc_buffer__clear(&machine->genbuf);
      gc_buffer__add_as_json(&machine->genbuf, machine->inflight.value, machine->inflight.type, machine);
      return (machine->genbuf.data);
    }
    return "error, no return value";
}

/*
char* getParam() {
    char* params="{\"x\":4,\"y\":5}";
    execGCFunctionParam("getParam", params, 12);
    if (machine->inflight.type != gc_type_null) {
      gc_buffer__clear(&machine->genbuf);
      gc_buffer__add_as_json(&machine->genbuf, machine->inflight.value, machine->inflight.type, machine);
      return (machine->genbuf.data);
    }
    return "error, no return value";
}
*/


void test() {
    print("Entering TEST\n");

    execGCFunction("test");

    readResultsFile();
    print("TEST DONE\n");
}

int main(i32_t argc, char **argv) {
    return 1;
}