all: clean compile update_greycat_so_accelerometer copy_greycat_to_device


clean:
	@rm -f -r build/
	@rm -f -r gcdata/
	@rm -f data.bin
	echo "CLEANING DONE\n"

compile:
	@greycat build
	@mkdir -p build && cd build && cmake .. && make && cd ..
	echo "GENERATING .so DONE\n"

update_greycat_so_accelerometer:
	@if [ ! -d " /home/oruas/AndroidStudioProjects/flutter_promesse/example/android/app/src/main/jniLibs/arm64-v8a/" ]; then\
            mkdir -p  /home/oruas/AndroidStudioProjects/flutter_promesse/example/android/app/src/main/jniLibs/arm64-v8a/; \
	fi
	@cp /home/oruas/AndroidStudioProjects/greycatLIB/build/libgreycatLIB.so /home/oruas/AndroidStudioProjects/flutter_promesse/example/android/app/src/main/jniLibs/arm64-v8a/libgreycatLIB.so

	@if [ ! -d " /home/oruas/AndroidStudioProjects/greycat_promesse/example/android/app/src/main/jniLibs/arm64-v8a/" ]; then\
            mkdir -p  /home/oruas/AndroidStudioProjects/greycat_promesse/example/android/app/src/main/jniLibs/arm64-v8a/; \
	fi
	@cp /home/oruas/AndroidStudioProjects/greycatLIB/build/libgreycatLIB.so /home/oruas/AndroidStudioProjects/greycat_promesse/example/android/app/src/main/jniLibs/arm64-v8a/libgreycatLIB.so

	@cp /home/oruas/AndroidStudioProjects/greycatLIB/build/libgreycatLIB.so /home/oruas/AndroidStudioProjects/greycat_benchmark_random/example/android/app/src/main/jniLibs/arm64-v8a/libgreycatLIB.so
	@cp /home/oruas/AndroidStudioProjects/greycatLIB/build/libgreycatLIB.so /home/oruas/AndroidStudioProjects/greycat_lppm/example/android/app/src/main/jniLibs/arm64-v8a/libgreycatLIB.so
	@cp /home/oruas/AndroidStudioProjects/greycatLIB/build/libgreycatLIB.so /home/oruas/AndroidStudioProjects/benchmark_acc/example/android/app/src/main/jniLibs/arm64-v8a/libgreycatLIB.so
	@cp /home/oruas/AndroidStudioProjects/greycatLIB/build/libgreycatLIB.so /home/oruas/AndroidStudioProjects/flutter_promesse/example/android/app/src/main/jniLibs/arm64-v8a/libgreycatLIB.so
	@echo "UPDATING .so IN GREYCAT_ACCELEROMETER DONE\n"

copy_greycat_to_device:
	@if [ -d "/run/user/1001/gvfs/mtp:host=Fairphone_FP3_A209G58B0202/" ]; then\
            echo "Fairphone found"; \
	    cp /home/oruas/AndroidStudioProjects/greycatLIB/gcdata/project.gcp /run/user/1001/gvfs/mtp\:host\=Fairphone_FP3_A209G58B0202/Internal\ shared\ storage/greycat/project.gcp; \
	fi
	@if [ -d "/run/user/1001/gvfs/mtp:host=Xiaomi_Redmi_Note_8_Pro_buw445r4o7c6sclf/" ]; then\
	    echo "Xiaomi found"; \
	    cp /home/oruas/AndroidStudioProjects/greycatLIB/gcdata/project.gcp /run/user/1001/gvfs/mtp\:host\=Xiaomi_Redmi_Note_8_Pro_buw445r4o7c6sclf/Internal\ shared\ storage/greycat/project.gcp; \
	fi
	@echo "UPDATING project.gcp ON DEVICE DONE\n"

test_main:
	rm -f -r gcdata/
	echo "CLEANING DONE\n"
	greycat run project.gcl



test_c:
	rm -f -r build/
	rm -f -r gcdata/
	rm -f data.bin
	echo "CLEANING DONE\n"
	greycat build
	echo "BUILDING DONE\n"
	mkdir -p build && cd build && cmake .. && make && cd ..
	./build/greycatLIB gcdata/project.gcp
	echo "RUNNING DONE\n"
